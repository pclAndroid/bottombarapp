package com.pcl.pclapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.iid.FirebaseInstanceId;
import com.pcl.pclapp.api.WebServiceHandler;
import com.pcl.pclapp.api.WebServiceResponse;
import com.pcl.pclapp.api_response.login.loginResponse.LoginResponse;
import com.pcl.pclapp.api_response.login.loginResponse.Result;
import com.pcl.pclapp.utils.GlobalUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        SmsBroadcastReceiver.OtpReceivedListener, GoogleApiClient.OnConnectionFailedListener, WebServiceResponse, View.OnClickListener{

    private static int RESOLVE_HINT = 2;
    @BindView(R.id.btn_submit)
    AppCompatButton btn_submit;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private EditText editText;
    private GoogleApiClient googleApiClient;
    private static String phoneNumber;
    int requestType = 1;
    List<Result> playerResult = new ArrayList<>();
    public static Boolean booltype;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set google api client for hint request

        SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        booltype=pref.getBoolean("Booltype", Boolean.parseBoolean(""));

        if(booltype){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }


        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(LoginActivity.this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
        editText = findViewById(R.id.phone_number_field);
        getHintPhoneNumber();
        ButterKnife.bind(this);
        btn_submit.setOnClickListener(this);
        editText.setOnFocusChangeListener((view, b) -> {
            if (b) {
                editText.setHint("Enter Mobile Number");
            } else {
                editText.setHint("******");
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onOtpReceived(String otp) {

      
    }

    @Override
    public void onOtpTimeout() {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private boolean validation() {
        String number = editText.getText().toString().trim();
        if (number.isEmpty()) {
            editText.setError("please enter 10 digit mobile number.");
            editText.requestFocus();
            return false;
        } else if (number.length() == 10) {
            phoneNumber = number;
        } else if (number.length() < 10) {
            editText.setError("please enter 10 digit mobile number.");
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public void getHintPhoneNumber() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();
        PendingIntent mIntent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(mIntent.getIntentSender(), RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Result if we want hint number
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                    phoneNumber = (credential.getId()).toString();
                    correctPhoneNumber(phoneNumber);
                }
            }
        }
    }
    public void correctPhoneNumber(String  str) {
        // String str = editText.getText().toString();
        str = str.replace("+91", "");
        str = str.replaceAll(" ", "");
        phoneNumber = str;
        editText.setText(phoneNumber);
    }

    @Override
    public void OnSuccess(Object object) {
        progressBar.setVisibility(View.GONE);
        if (requestType == 1 && object instanceof LoginResponse) {
            LoginResponse loginResponse = (LoginResponse)object;
            String status = loginResponse.getStatus();
            playerResult = new ArrayList<>();
            playerResult = loginResponse.getResults();

                            GlobalUtils.showToast(getApplicationContext(),status);
                playerResult = loginResponse.getResults();

                booltype=true;
                String id  = playerResult.get(0).getId();
                String team_id = playerResult.get(0).getTeamId();
                String name  = playerResult.get(0).getPlayerName();
                String team_name = playerResult.get(0).getTeamName();
                String wickets = String.valueOf(playerResult.get(0).getTotalWkts());
                String total_runs = playerResult.get(0).getTotalRuns();

                String total_matches = "100";
                String total_balls = "800";
                String profile_pic = "http://procorporateleague.com/wp-includes/images/pcl/Player%20Gallery/"+team_id+"/"+id+".png";

                SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("id", id);
                edit.putString("team_id", team_id);
                edit.putString("name", name);
                edit.putString("team_name", team_name);
                edit.putString("wickets", wickets);
                edit.putString("totalruns", total_runs);
                edit.putString("totalmatches", total_matches);
                edit.putBoolean("Booltype",booltype);
                edit.putString("totalballs", total_balls);
                edit.putString("picurl",profile_pic);
                edit.apply();

            Intent intent = new Intent(LoginActivity.this, VerifyPhoneActivity.class);
            intent.putExtra("mobile", phoneNumber);
            startActivity(intent);

        }
    }

    @Override
    public void OnFailure() {
        progressBar.setVisibility(View.GONE);
        GlobalUtils.showToast(getApplicationContext(),"login failed");
    }

    @Override
    public void onErrorHandling(Response response) {
        progressBar.setVisibility(View.GONE);
        GlobalUtils.showToast(getApplicationContext(),"login failed");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                //Do this and this        break;
                if (GlobalUtils.isNetworkAvailable(this)) {
                    if (validation()) {

                        playerLogin();
                    }
                } else {
                    GlobalUtils.showToast(this, getString(R.string.no_internet));
                }
            default: //For all other cases, do this        break;
        }

    }

    //retrofit use for login
    public void playerLogin() {
        progressBar.setVisibility(View.VISIBLE);
        if (GlobalUtils.isNetworkAvailable(this)) {

            new WebServiceHandler().playerLoginStats(this, phoneNumber);
            requestType = 1;
        }
    }

}

