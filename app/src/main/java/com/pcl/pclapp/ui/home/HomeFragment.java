package com.pcl.pclapp.ui.home;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import com.mikhaellopez.circularimageview.CircularImageView;
import com.pcl.pclapp.R;
import com.pcl.pclapp.WebviewActivity;
import com.pcl.pclapp.WelcomeGuestActivity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import static com.pcl.pclapp.LoginActivity.booltype;

public class HomeFragment extends Fragment {

   // private HomeViewModel homeViewModel;
   private CircularImageView profileimg;
    private TextView nametxt,teamnametxt,wickts,runs,matchs,balls;
    private String id,teamid,name,teamname,wickets,totalruns,totalmatchs,totalballs,profilepic;

    private TextView dontrecotxt;
    AppCompatButton btnSubmit;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        SharedPreferences pref = getActivity().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        id = pref.getString("id","");
        //booltype=pref.getBoolean("Booltype", Boolean.parseBoolean(""));
        teamid = pref.getString("team_id","");
        name = pref.getString("name","");
        teamname = pref.getString("team_name","");
        wickets = pref.getString("wickets","");
        totalruns = pref.getString("totalruns","");
        totalmatchs = pref.getString("totalmatches","");
        totalballs = pref.getString("totalballs","");
        profilepic = pref.getString("picurl","");

       // Log.e("data",id);


        profileimg = (CircularImageView)root.findViewById(R.id.iv_player_image);
        nametxt = (TextView)root.findViewById(R.id.text_name);
        teamnametxt = (TextView)root.findViewById(R.id.text_company);
        wickts = (TextView)root.findViewById(R.id.text_player_wicket);
        runs = (TextView)root.findViewById(R.id.text_player_run);
        matchs = (TextView)root.findViewById(R.id.text_player_match);
        balls = (TextView)root.findViewById(R.id.text_player_ball);

        dontrecotxt = (TextView)root.findViewById(R.id.recogtext);

        Picasso.get().load(profilepic).networkPolicy(NetworkPolicy.NO_CACHE).into(profileimg);


        nametxt.setText(name);
        teamnametxt.setText(teamname);
        wickts.setText(wickets);
        runs.setText(totalruns);
        matchs.setText(totalmatchs);
        balls.setText(totalballs);


        btnSubmit = (AppCompatButton)root.findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("id",id);
                //startActivity(new Intent(WelcomeGuestActivity.this, RegisterActivity.class));
                Intent intent = new Intent(getActivity(), WebviewActivity.class);
                startActivity(intent);
            }
        });

        dontrecotxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WelcomeGuestActivity.class);
                getActivity().startActivity(intent);
            }
        });
//        final TextView textView = root.findViewById(R.id.text_home);
//        homeViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        return root;
    }
}