package com.pcl.pclapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.WindowManager;

import com.pcl.pclapp.ui.matches.PlayerAdapter;
import com.pcl.pclapp.ui.matches.PlayerModel;

import java.util.ArrayList;
import java.util.List;

public class MergeActivity extends AppCompatActivity {

    List<MergeModel> productList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        recyclerView = (RecyclerView)findViewById(R.id.recy2);

        //recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setHasFixedSize(true);

        productList = new ArrayList<>();

        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));
        productList.add(new MergeModel("Ashwini Kumar","NIIT Technologies","Join Date - 05-09-2018","55",
                "1500","1600","90",R.drawable.img1));


        MergeAdapter adapter = new MergeAdapter(this, productList);

        //setting adapter to recyclerview
        recyclerView.setAdapter(adapter);
    }
}
