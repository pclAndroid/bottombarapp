package com.pcl.pclapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.pcl.pclapp.ui.matches.PlayerAdapter;
import com.pcl.pclapp.ui.matches.PlayerModel;

import java.util.List;

public class MergeAdapter extends RecyclerView.Adapter<MergeAdapter.ProductViewHolder> {

    private Context mCtx;
    private List<MergeModel> productList;

    public MergeAdapter(Context mCtx, List<MergeModel> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public MergeAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.merge_layout, null);
        return new MergeAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MergeAdapter.ProductViewHolder holder, int position) {

        MergeModel product = productList.get(position);

        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(product.getImage()));
        holder.name.setText(product.getName());
        holder.cname.setText(product.getCompname());
        holder.joindate.setText(product.getJoindate());
        holder.matchtxt.setText(product.getMatch());
        holder.runtxt.setText(product.getRun());
        holder.boltxt.setText(product.getBall());
        holder.wktstxt.setText(product.getWickets());


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name, cname,joindate,matchtxt,runtxt,boltxt,wktstxt;

        public ProductViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.iv_player_image);
            name = itemView.findViewById(R.id.text_name);
            cname = itemView.findViewById(R.id.text_company);
            joindate = itemView.findViewById(R.id.joindatetxt);
            matchtxt = itemView.findViewById(R.id.text_player_match);
            runtxt = itemView.findViewById(R.id.text_player_run);
            boltxt = itemView.findViewById(R.id.text_player_ball);
            wktstxt = itemView.findViewById(R.id.text_player_wicket);
        }
    }
}

