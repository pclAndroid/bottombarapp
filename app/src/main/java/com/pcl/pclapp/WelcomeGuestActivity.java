package com.pcl.pclapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pcl.pclapp.api.WebServiceHandler;
import com.pcl.pclapp.api.WebServiceResponse;
import com.pcl.pclapp.api_response.login.loginResponse.LoginResponse;
import com.pcl.pclapp.api_response.login.loginResponse.Result;
import com.pcl.pclapp.utils.GlobalUtils;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WelcomeGuestActivity extends AppCompatActivity implements WebServiceResponse, AdapterView.OnItemSelectedListener {

    AppCompatButton btnSubmit, skipbtn;
    EditText fname, lname;

    private SearchableSpinner spinerTeam;
    private ArrayList<String> team =new ArrayList<String>();
    private ArrayList<String> teamid =new ArrayList<String>();
    String TeamID;
    private String id;

    private  int requestType;
    List<Result> playerResult = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_guest);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        fname = (EditText)findViewById(R.id.editTextfnm);
        lname = (EditText)findViewById(R.id.editTextlnm);

        spinerTeam = (SearchableSpinner) findViewById(R.id.teamname);
        spinerTeam.setOnItemSelectedListener(this);
        getDataInd();

        btnSubmit = (AppCompatButton)findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidate())
                {
                    Submit();
                }
            }
        });

        skipbtn = (AppCompatButton)findViewById(R.id.btn_skip);
        skipbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("id",id);
                //startActivity(new Intent(WelcomeGuestActivity.this, RegisterActivity.class));
                Intent intent = new Intent(WelcomeGuestActivity.this, WebviewhomeActivity.class);
                startActivity(intent);
            }
        });

    }

    private boolean isValidate()
    {

//        if (spinerTeam.getSelectedItem()=){
//            spinerTeam.requestFocus();
//            return false;
//        }

        if (fname.getText().toString().length() == 0) {
            fname.setError("Please enetr First Name");
            fname.requestFocus();
            return false;
        }

        if (lname.getText().toString().length() == 0) {
            lname.setError("Please enetr Last Name");
            lname.requestFocus();
            return false;
        }
//        if (teamname1.getText().toString().length() == 0) {
//            teamname1.setError("Please select team name");
//            teamname1.requestFocus();
//            return false;
//        }

        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        SearchableSpinner spinner = (SearchableSpinner) parent;

        if(spinner.getId() == R.id.teamname)
        {
            TeamID=teamid.get((int) id);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    private void  getDataInd() {

        StringRequest stringRequest=new StringRequest(Request.Method.GET, "http://procorporateleague.com/pcl_rest/api/v1/tournaments/TeamsPoints/all",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Log.e("rootJsonArray",response);
                        try{
                            JSONObject rootJsonObject = new JSONObject(response);
                            JSONArray subCategoryArray = rootJsonObject.getJSONArray("results");

                            for(int i=0;i<subCategoryArray.length();i++){
                                JSONObject jsonObject1=subCategoryArray.getJSONObject(i);
                                String tmid=jsonObject1.getString("team_id");
                                String teams=jsonObject1.getString("team_name");
                                teamid.add(tmid);
                                team.add(teams);
                            }

                            spinerTeam.setAdapter(new ArrayAdapter<String>(WelcomeGuestActivity.this, R.layout.spinneritems, team));
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    private void Submit()
    {

        final String firstname = fname.getText().toString().trim();
        final String lastname = lname.getText().toString().trim();

        //Log.e("tid",TeamID);

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("fName",firstname);
        hashMap.put("team_id",TeamID);

        if (GlobalUtils.isNetworkAvailable(this)) {
            new WebServiceHandler().GetPlayerDetails(this, hashMap);
            requestType = 1;
        }
        else{
            //Toast.makeText()
        }
    }

    @Override
    public void OnSuccess(Object object) {
        if (requestType == 1 && object instanceof LoginResponse) {
            LoginResponse loginResponse = (LoginResponse) object;
            String status = loginResponse.getStatus();
            playerResult = new ArrayList<>();
            playerResult = loginResponse.getResults();
           // Log.e("phone", String.valueOf(playerResult));

            if (playerResult.size() == 0) {
                //Log.e("phone",phoneNumber);

                startActivity(new Intent(WelcomeGuestActivity.this, RegisterActivity.class));
            }
            if (playerResult.size() == 1) {
                GlobalUtils.showToast(getApplicationContext(), status);
                //playerResult = loginResponse.getResults();

                 id = playerResult.get(0).getId();
                String team_id = playerResult.get(0).getTeamId();
                String name = playerResult.get(0).getPlayerName();
                String team_name = playerResult.get(0).getTeamName();
                String wickets = String.valueOf(playerResult.get(0).getTotalWkts());
                String total_runs = playerResult.get(0).getTotalRuns();

                String total_matches = "100";
                String total_balls = "800";
                String profile_pic = "http://procorporateleague.com/wp-includes/images/pcl/Player%20Gallery/" + team_id + "/" + id + ".png";
                // Log.e("url",profile_pic);

                SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("id", id);
                edit.putString("team_id", team_id);
                edit.putString("name", name);
                edit.putString("team_name", team_name);
                edit.putString("wickets", wickets);
                edit.putString("totalruns", total_runs);
                edit.putString("totalmatches", total_matches);
                edit.putString("totalballs", total_balls);
                edit.putString("picurl", profile_pic);
                edit.apply();
                Intent intent = new Intent(WelcomeGuestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

    }

    @Override
    public void OnFailure() {

        GlobalUtils.showToast(getApplicationContext(),"login failed");
    }

    @Override
    public void onErrorHandling(retrofit2.Response response) {

        GlobalUtils.showToast(getApplicationContext(),"login failed");
    }



}
