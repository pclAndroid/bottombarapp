package com.pcl.pclapp.api;


import com.pcl.pclapp.api_response.login.loginResponse.LoginResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestInterface {

    @GET("players/stats/{playerMobile}")
    Call<LoginResponse> playerLogin(@Path("playerMobile") String playerMobile);

    @POST("tournaments/getPlayerDetails")
    Call<LoginResponse> playerGetDetails(@Body Map<String, String> playerData);

}

