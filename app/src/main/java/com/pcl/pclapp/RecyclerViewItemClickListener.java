package com.pcl.pclapp;

import android.view.View;

public interface RecyclerViewItemClickListener {
    public void onClick(View view, int position);
}
