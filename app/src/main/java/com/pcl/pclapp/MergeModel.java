package com.pcl.pclapp;

public class MergeModel  {

    private String name;
    private String Compname;
    private String joindate;
    private String match;
    private String run;
    private String ball;
    private String wickets;
    private int image;


    public MergeModel(String name, String compname, String joindate, String match, String run, String ball, String wickets, int image) {
        this.name = name;
        Compname = compname;
        this.joindate = joindate;
        this.match = match;
        this.run = run;
        this.ball = ball;
        this.wickets = wickets;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompname() {
        return Compname;
    }

    public void setCompname(String compname) {
        Compname = compname;
    }

    public String getJoindate() {
        return joindate;
    }

    public void setJoindate(String joindate) {
        this.joindate = joindate;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getBall() {
        return ball;
    }

    public void setBall(String ball) {
        this.ball = ball;
    }

    public String getWickets() {
        return wickets;
    }

    public void setWickets(String wickets) {
        this.wickets = wickets;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
