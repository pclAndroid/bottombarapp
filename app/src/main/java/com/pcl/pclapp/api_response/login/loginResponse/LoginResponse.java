package com.pcl.pclapp.api_response.login.loginResponse;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class LoginResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("results")

    private List<Result> results = null;
    @SerializedName("message")

    private String message;
    @SerializedName("code")
    private Integer code;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
